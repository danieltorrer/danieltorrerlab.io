var buttonEl = document.querySelector('.button');

buttonEl.addEventListener('click', function(e) {
	e.preventDefault();
	setTimeout(() => {
		var page = e.srcElement.href;
		window.location = page;
	}, 550);
});
